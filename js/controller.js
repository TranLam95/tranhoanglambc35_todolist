
export let layThongTinTuForm = () => {
    const tenCv = document.getElementById("newTask").value;
    let cv = {
        ten: tenCv,
    }
    return cv;
}
export let renderDscvTodo = (list) => {
    let contentHTML = "";
    list.forEach((cv) => {
        contentHTML += `<li>
        <tr>
        <div>
        <span>${cv.ten}</span>
        </div>
        <div>
        <button onclick="xoaCv('${cv.ten}')"><i class="fa fa-trash-alt"></i></button>
        <button onclick="hoanThanhCv('${cv.ten}')"><i class="fa fa-check-circle"></i></button>
        </div>
        </tr>
        </li>`;
    });
    document.getElementById("todo").innerHTML = contentHTML;
}

export let renderDscvCompleted = (list) => {
    let content = "";
    list.forEach((cv) => {
        content += `<li>
        <tr>
        <div>
        <span>${cv.ten}</span>
        </div>
        <div>
        <button onclick="xoaCvComplete('${cv.ten}')"><i class="fa fa-trash-alt"></i></button>
        <button id="clickcomplete"><i class="fa fa-check-circle"></i></button>
        </div>
        </tr>
        </li>`;
    });
    document.getElementById("completed").innerHTML = content;
}

export let sapXepAZ = (list, item) => {
    var swapped;
    do {
        swapped = false;
        for (var i = 0; i < list.length - 1; i++) {
            if (list[i][item] > list[i + 1][item]) {
                var temp = list[i];
                list[i] = list[i + 1];
                list[i + 1] = temp;
                swapped = true;
            }
        }
    } while (swapped);
}

export let sapXepZA = (list, item) => {
    var swapped;
    do {
        swapped = false;
        for (var i = 0; i < list.length - 1; i++) {
            if (list[i][item] < list[i + 1][item]) {
                var temp = list[i];
                list[i] = list[i + 1];
                list[i + 1] = temp;
                swapped = true;
            }
        }
    } while (swapped);
}