import { layThongTinTuForm, renderDscvTodo, renderDscvCompleted, sapXepAZ, sapXepZA } from "./controller.js";

const DSCVTODO = "DSCVTODO";
const DSCVCOMPLETED = "DSCVCOMPLETED"
let dscvTodo =[];
let dscvCompleted = [];
let dataJson = localStorage.getItem(DSCVTODO);
let dataJsonCompleted = localStorage.getItem(DSCVCOMPLETED)
if(dataJson){
    dscvTodo = JSON.parse(dataJson);
    renderDscvTodo(dscvTodo);
}
if(dataJsonCompleted){
    dscvCompleted = JSON.parse(dataJsonCompleted);
    renderDscvCompleted(dscvCompleted);
}

function saveLocalStorage(){
    let dscvJson = JSON.stringify(dscvTodo);
    localStorage.setItem(DSCVTODO, dscvJson);
    renderDscvTodo(dscvTodo);
    let dscvcompletedJSon = JSON.stringify(dscvCompleted);
    localStorage.setItem(DSCVCOMPLETED, dscvcompletedJSon);
    renderDscvCompleted(dscvCompleted);
}

// thêm công việc
document.getElementById("addItem").addEventListener("click", function(){
    let data = layThongTinTuForm();
    dscvTodo.push(data);
    renderDscvTodo(dscvTodo);
    saveLocalStorage();
})
// xóa công việc
function xoaCv(idCv){
    let index = dscvTodo.findIndex(function(cv){
        return cv.ten == idCv;
    });
    if (index == -1){
        return;
    }
    dscvTodo.splice(index,1);
    renderDscvTodo(dscvTodo);
    saveLocalStorage();
}
window.xoaCv = xoaCv;

function xoaCvComplete(idCv){
    let index = dscvCompleted.findIndex(function(cv){
        return cv.ten == idCv;
    });
    if (index == -1){
        return;
    }
    dscvCompleted.splice(index,1);
    renderDscvCompleted(dscvCompleted);
    saveLocalStorage();
}
window.xoaCvComplete = xoaCvComplete;

// thêm công việc hoàn thành
function hoanThanhCv(idCv){
    let index = dscvTodo.findIndex(function(cv){
        return cv.ten == idCv;
    });
    if (index == -1){
        
        return;
    }
    var tenIndex = dscvTodo[index];
    dscvTodo.splice(index,1);
    dscvCompleted.push(tenIndex);
    renderDscvCompleted(dscvCompleted);
    renderDscvTodo(dscvTodo);
    saveLocalStorage();
}
window.hoanThanhCv = hoanThanhCv;

// sắp xếp theo thứ tự từ a-z
document.getElementById("two").addEventListener("click", function(){
    sapXepAZ(dscvTodo, 'ten')
    sapXepAZ(dscvCompleted, 'ten')
    renderDscvCompleted(dscvCompleted);
    renderDscvTodo(dscvTodo);
    saveLocalStorage();
});
// sắp xếp theo thứ tự từ z-a
document.getElementById("three").addEventListener("click", function(){
    sapXepZA(dscvTodo, 'ten');
    sapXepZA(dscvCompleted, 'ten');
    renderDscvCompleted(dscvCompleted);
    renderDscvTodo(dscvTodo);
    saveLocalStorage();
});

